import { connect } from 'react-redux';
import VideoPane from '../components/VideoPane';
import * as app from '../modules/app';

const mapStateToProps = (state) => {
  const {
    buttonEnabled,
  } = state.app;

  return {
    buttonEnabled,
  };
};

const mapDispatchToProps = dispatch => ({
  setButtonEnabled: buttonEnabled => dispatch(app.setButtonEnabled(buttonEnabled)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(VideoPane);
