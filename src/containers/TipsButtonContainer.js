import { connect } from 'react-redux';
import TipsButton from '../elements/TipsButton';

const mapStateToProps = (state) => {
  const {
    buttonEnabled,
  } = state.app;

  return {
    buttonEnabled,
  };
};

export default connect(
  mapStateToProps,
  null,
)(TipsButton);
