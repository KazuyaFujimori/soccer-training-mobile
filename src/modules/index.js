import { combineReducers } from 'redux';
import app from './app';
import video from './video';

const reducers = combineReducers({
  app,
  video,
});

export default reducers;
