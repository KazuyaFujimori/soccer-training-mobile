import * as constants from '../constants/index';

export const setButtonEnabled = buttonEnabled => ({
  type: constants.SET_BUTTON_ENABLED,
  payload: {
    buttonEnabled,
  },
});

const INITIAL_STATE = {
};

const reducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case constants.SET_BUTTON_ENABLED: {
      const { payload } = action;
      const { buttonEnabled } = payload;
      return { ...state, buttonEnabled };
    }
    default:
      return state;
  }
};

export default reducer;
